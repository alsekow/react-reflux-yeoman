var React = require('react');
var Reflux = require('reflux');
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;
var Grid = require('react-bootstrap').Grid;
var Link = require('react-router').Link;

var DetailsStore = require('../stores/DetailsStore');
var DetailsActions = require('../actions/DetailsActions');

var ProductDetails = React.createClass({
  mixins: [
    // connecting to the store will automatically update component's state with new data when it becomes available
    Reflux.connect(DetailsStore)
  ],

  renderReviews: function(review, i) {
    return(
      <li style={{listStyleType: 'none'}} key={i}>
        <h4>Reviewer: {review.Reviewer}</h4>
        <span style={{float: 'right'}}>Rating: {review.Rating}</span>
        <p>{review.Description}</p>
      </li>
    )
  },

  handleSuccess: function() {
    return (
      <div className="detailsContainer">
        <Row>
          <Col md={10}>
            <h2>{this.state.details.Name}</h2>
          </Col>
          <Col md={2}>
            <h3>£{this.state.details.PriceDecimal.toFixed(2)}</h3>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <img src={this.state.details.ImageUrl} alt={this.state.details.Name} className="thumbnail" />
          </Col>
          <Col md={8}>
            {this.state.details.Description}
          </Col>
        </Row>
        <Row>
          <Col md={8}>
            <h2>Reviews:</h2>
            <ul>
            {this.state.details.Reviews !== null ? this.state.details.Reviews.map(this.renderReviews) : 'No reviews for this product.'}
            </ul>
          </Col>
        </Row>
      </div>
    );
  },

  handleError: function(id) {
    return (
      <Grid>
        <Row>
          <div className="error">
            {this.state.msg}
          </div>
        </Row>
      </Grid>
    )
  },

  componentDidMount() {
    DetailsActions.loadData(this.props.params.id);
  },

  render: function() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Link to="/">Back to Product list</Link>
          </Col>
        </Row>
        {this.state.msg == '' ? this.handleSuccess() : this.handleError()}
      </Grid>
    )
  }
});

module.exports = ProductDetails;
