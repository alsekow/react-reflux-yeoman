var React = require('react');
var Link = require('react-router').Link;
var Grid = require('react-bootstrap').Grid;
var Row = require('react-bootstrap').Row;

var NotFound = React.createClass({

  render: function() {

    return (
      <Grid>
        <Row>
          <h1>Path not Found :(</h1>
          <Link to="/">Back to home</Link>
        </Row>
      </Grid>
    );
  }
});

module.exports = NotFound;
