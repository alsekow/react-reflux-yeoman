var React = require('react');
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;
var Grid = require('react-bootstrap').Grid;

var ProductList = require('./productList.jsx')

var Home = React.createClass({

  render: function() {

    return (
      <Grid>
        <Row>
          <Col md={12}>
            <h1>
              Digiterre storefront
            </h1>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <ProductList />
          </Col>
        </Row>
      </Grid>
    );
  }
});


module.exports = Home;
