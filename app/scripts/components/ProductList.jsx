var React = require('react');
var Reflux = require('reflux');
var Navigation = require('react-router').Navigation;
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;

var ProductsStore = require('../stores/ProductsStore');
var ProductsActions = require('../actions/ProductsActions');


var ProductList = React.createClass({
  mixins: [
    Navigation,
    // connecting to the store will automatically update component's state with new data when it becomes available
    Reflux.connect(ProductsStore)
  ],

  handleClick(id) {
    this.transitionTo('/product/'+id);
  },

  renderItem(item, i) {
    return(
      <div className="cell"
        key={i}
        onClick={this.handleClick.bind(this, item.Id)}>
        <Row>
          <Col md={12}>
            <img src={item.ImageUrl} alt={item.Name} className="thumbnail" />
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <span>
              {item.Name}
            </span>
            <span style={{float: 'right'}}>
              £{item.PriceDecimal.toFixed(1)}
            </span>
          </Col>
        </Row>
      </div>
    )
  },

  componentDidMount() {
    ProductsActions.loadList();
  },

  render() {
    return (
      <div>
        {typeof this.state.list !== 'undefined' && this.state.list.length > 0 ? this.state.list.map(this.renderItem) : this.state.msg}
      </div>
    );
  }
});

module.exports = ProductList;
