var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var DefaultRoute = Router.DefaultRoute;


var ProductDetails = require('./components/productDetails');
var Layout = require('./components/layout');
var Home = require('./components/home');
var NotFound = require('./components/notfound');



var routes = (
  <Route name="layout" path="/" handler={Layout}>
    <Route path="" handler={Home} />
    <Route path="product/:id" handler={ProductDetails} />
    <NotFoundRoute handler={NotFound}/>
  </Route>
);

exports.start = function() {

  Router.run(routes, function (Handler) {
    React.render(<Handler />, document.getElementById('content'));
  });
}
