var Reflux = require('reflux');
var Q = require('q');
var $ = require('jquery');

var config = require('../config');


var apiCall = function() {
  var deferred = Q.defer();

  return $.ajax({
    url: config.API_ENTRYPOINTS.productsList,
    beforeSend: function( xhr ) {
      xhr.setRequestHeader("Authorization", config.AUTH_KEY);
    }
  });
};

var ProductsActions = Reflux.createActions({
  loadList: {children: ["completed","failed"]}
});

ProductsActions.loadList.listen(function() {

  apiCall()
    .done(this.completed)
    .fail(this.failed);
});

module.exports = ProductsActions;
