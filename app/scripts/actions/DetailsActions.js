var Reflux = require('reflux');
var Q = require('q');
var $ = require('jquery');

var config = require('../config');

// this API call returns a promise which will decide which action to trigger in the store.
var apiCall = function(id) {
  var deferred = Q.defer();

  return $.ajax({
    url: config.API_ENTRYPOINTS.productsList + id,
    beforeSend: function( xhr ) {
      xhr.setRequestHeader("Authorization", config.AUTH_KEY);
    }
  });
};

var DetailsActions = Reflux.createActions({
  loadData: {children: ["completed","failed"]}
});


DetailsActions.loadData.listen(function(id) {
  apiCall(id)
    .done(this.completed)
    .fail(this.failed);
});


module.exports = DetailsActions;
