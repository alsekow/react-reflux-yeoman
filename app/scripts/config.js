
const API_PATH = 'http://frontenddevinterview.azurewebsites.net/api/';
const API_ENTRYPOINTS = {
  productsList: API_PATH + '/products/'
};
const AUTH_KEY = 'Basic QWxla3NhbmRlciBTZWtvd3NraQ==';



const config = {
  API_ENTRYPOINTS,
  AUTH_KEY
};


module.exports = config;
