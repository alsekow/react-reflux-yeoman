var Reflux = require('reflux');

var DetailsActions = require('../actions/DetailsActions');


var DetailsStore = Reflux.createStore({
  listenables: [DetailsActions],

  getInitialState() {
    return {
      details: {},
      msg: 'Loading...'
    }
  },

  loadData() {
    this.trigger({msg: 'Loading...'});
  },

  // handling API errors
  loadDataFailed(res) {
    var status = res.statusText || '';
    this.msg = 'Retreiving product data failed: ' + status;
    this.trigger({msg: this.msg}); // print error, but don't do anything else.
  },

  loadDataCompleted(details) {
    this.details = details;
    this.msg = '';
    this.trigger({details: details, msg: this.msg});
  }

});


module.exports = DetailsStore;

