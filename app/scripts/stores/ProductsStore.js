var Reflux = require('reflux');

var ProductsActions = require('../actions/ProductsActions');


var ProductsStore = Reflux.createStore({
  listenables: [ProductsActions],

  getInitialState() {
    return {
      list: [],
      msg: ''
    }
  },

  loadList(msg) {
    this.msg = 'Loading...';
    this.trigger({msg: this.msg});
  },

  // handling API errors
  loadListFailed(msg) {
    this.msg = 'Retreiving product data failed: ' + msg.statusText + '. I will give it another try in 5 seconds.';

    // print error, then retry to get the product list.
    this.trigger({msg: this.msg});
    setTimeout(function() {
      ProductsActions.loadList();
    }, 5000)
  },

  loadListCompleted(list) {
    this.trigger({list: list});
  }
});


module.exports = ProductsStore;
