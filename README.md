# digiterre challange


Scaffolding with yeoman

Created with react-reflux generator




### To run:
* clone this repo
* run gulp
* or, to run the code in the browser, open dist/index.html



### OPTIONAL: To run build / dev:
* run npm install (if there are contextify errors, ignore for now)
* If npm install does not work for some reason try npm install -f
* run 'gulp webserver'

* to run dev build 'gulp serve'. Dev build might not work on node version mismatch(4.2.2) or lack of other dependancies




### Stack information:
* react with reflux, react-bootstrap and react-router
* gulp with: eslint, browserify, reactify, jest provided by generator
* bower with bootstrap for CSS.
