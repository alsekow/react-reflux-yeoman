jest.dontMock('../app/scripts/components/ProductList.jsx');

describe('Product list', function() {
  it('shows cells', function() {
    var React = require('react/addons');
    var ProductList = require('../app/scripts/components/ProductList.jsx');
    var TestUtils = React.addons.TestUtils;

    // Render the home component
    var list = TestUtils.renderIntoDocument(
      <ProductList />
    );

    // Verify that the title is there
    var cells = TestUtils.findRenderedDOMComponentWithClass(list, '.cell');
    expect(cells.getDOMNode()).toBe(true);
  });
});
