jest.dontMock('../app/scripts/components/ProductDetails.jsx');

describe('Details page', function() {
  it('shows details', function() {
    var React = require('react/addons');
    var ProductDetails = require('../app/scripts/components/ProductDetails.jsx');
    var TestUtils = React.addons.TestUtils;

    var details = TestUtils.renderIntoDocument(
      <ProductDetails />
    );

    var details = TestUtils.findRenderedDOMComponentWithClass(details, '.detailsContainer');
    expect(details.getDOMNode()).toBePresent();
  });
});
